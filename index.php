<?php include('header.php'); ?>
      <div class="row">
        <div class="col-md-12 px-0">
          <div class="carousel slide" id='slider' data-ride="carousel">
            <div class="carousel-inner">
              <div class="carousel-item active">
                <img src="https://www.hdwallpapers.in/download/bicycle-1280x800.jpg" alt="">
              </div>
              <div class="carousel-item">
                <img src="https://www.hdwallpapers.in/download/judge_harry_pregerson_interchange_los_angeles_highway_4k-1080x1920.jpg" alt="">
              </div>
            </div>
            <!-- Icons -->
            <a href="#slider" class='carousel-control-prev' data-slide="prev">
              <span class='carousel-control-prev-icon'></span>
            </a>
            <a href="#slider" class='carousel-control-next' data-slide="next">
              <span class='carousel-control-next-icon'></span>
            </a>
            <!-- Icons end -->
          </div>
        </div>
      </div>
      <!-- row 2 End  -->
      <div class="row">
        <div class="col-md-4">
          <h1 class='text-center mt-4'><i class="fab fa-500px"></i></h1>
          <h2 class='text-center'>About Us</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
        </div>
        <div class="col-md-4">
          <h1 class='text-center mt-4'><i class="fab fa-500px"></i></h1>
          <h2 class='text-center'>Why Us</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
        </div>
        <div class="col-md-4">
          <h1 class='text-center mt-4'><i class="fab fa-500px"></i></h1>
          <h2 class='text-center'>Contact Us</h2>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</p>
        </div>
      </div>
      <!-- row 3 End -->
<?php include('footer.php'); ?>
