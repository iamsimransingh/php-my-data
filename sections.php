<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title></title>
    <meta name="viewport" content="width=device-width" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
    <style media="screen">
      html, body{
        height:100%;
      }
    </style>
  </head>
  <body>
    <nav class='navbar fixed-top navbar-expand-md'>
      <ul class="navbar-nav ">
        <li class='nav-item'>
          <a href="#sec1" class='nav-link'>Dark</a>
        </li>
        <li class='nav-item'>
          <a href="#sec2" class='nav-link'>Info</a>
        </li>
        <li class='nav-item'>
          <a href="#sec3" class='nav-link'>Success</a>
        </li>
      </ul>
    </nav>
    <div class="container-fluid h-100">
      <div class="row h-100">
        <div class="col-md-12 h-100 bg-dark" id='sec1'>

        </div>
      </div>
      <div class="row h-100">
        <div class="col-md-12 h-100 bg-info" id='sec2'>

        </div>
      </div>
      <div class="row h-100">
        <div class="col-md-12 h-100 bg-success" id='sec3'>

        </div>
      </div>
    </div>
  </body>
</html>
